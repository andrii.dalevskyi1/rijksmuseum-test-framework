# rijksmuseum-test-framework

This is a demo framework for testing `https://www.rijksmuseum.nl/api/` API.

## How to run tests

- Use for it Build -> Pipelines -> Run Pipeline
- After the test run is finished you can browse the report from the job artifacts. Main page is located under `target/site/serenity/indes.html`
