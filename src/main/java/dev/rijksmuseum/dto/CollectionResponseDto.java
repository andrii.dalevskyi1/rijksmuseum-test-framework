package dev.rijksmuseum.dto;

import dev.rijksmuseum.dto.artObject.ArtObjectDto;
import dev.rijksmuseum.dto.artObject.CountFacetsDto;
import dev.rijksmuseum.dto.artObject.FacetDto;
import lombok.Data;

import java.util.List;

@Data
public class CollectionResponseDto {
    private Integer elapsedMilliseconds;
    private Integer count;
    private CountFacetsDto countFacets;
    private List<ArtObjectDto> artObjects;
    private List<FacetDto> facets;
}
