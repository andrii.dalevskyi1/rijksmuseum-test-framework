package dev.rijksmuseum.dto;

import dev.rijksmuseum.dto.artObject.ArtObjectDto;
import dev.rijksmuseum.dto.artObjectPage.ArtObjectPageDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(exclude = {"elapsedMilliseconds"})
public class CollectionDetailResponseDto {
    private Integer elapsedMilliseconds;
    private ArtObjectDto artObject;
    private ArtObjectPageDto artObjectPage;
}
