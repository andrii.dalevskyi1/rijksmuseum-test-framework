package dev.rijksmuseum.dto;

import lombok.Data;

@Data
public class ErrorResponseDto {
    private String type;
    private String title;
    private Integer status;
    private String traceId;
}
