package dev.rijksmuseum.dto.artObject;

import lombok.Data;

import java.util.List;

@Data
public class ClassificationDto {
    private List<String> iconClassIdentifier;
    private List<String> iconClassDescription;
    private List<Object> motifs;
    private List<Object> events;
    private List<Object> periods;
    private List<Object> places;
    private List<Object> people;
    private List<String> objectNumbers;
}
