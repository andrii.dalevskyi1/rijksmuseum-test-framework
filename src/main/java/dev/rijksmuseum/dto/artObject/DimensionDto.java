package dev.rijksmuseum.dto.artObject;

import lombok.Data;

@Data
public class DimensionDto {
    private String unit;
    private String type;
    private Object precision;
    private Object part;
    private String value;
}
