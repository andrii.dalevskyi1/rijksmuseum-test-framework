package dev.rijksmuseum.dto.artObject;

import lombok.Data;

@Data
public class AcquisitionDto {
    private String method;
    private String date;
    private String creditLine;
}
