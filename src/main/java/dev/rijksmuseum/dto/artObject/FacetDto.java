package dev.rijksmuseum.dto.artObject;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class FacetDto {
    private List<Map<String, Object>> facets;
    private String name;
    private Integer otherTerms;
    private Integer prettyName;
}
