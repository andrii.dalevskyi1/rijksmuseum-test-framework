package dev.rijksmuseum.dto.artObject;

import lombok.Data;

@Data
public class ImageDto {
    private String guid;
    private Integer offsetPercentageX;
    private Integer offsetPercentageY;
    private Integer width;
    private Integer height;
    private String url;
}
