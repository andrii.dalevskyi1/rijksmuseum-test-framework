package dev.rijksmuseum.dto.artObject;

import lombok.Data;

@Data
public class LabelDto {
    private String title;
    private String makerLine;
    private String description;
    private String notes;
    private String date;
}
