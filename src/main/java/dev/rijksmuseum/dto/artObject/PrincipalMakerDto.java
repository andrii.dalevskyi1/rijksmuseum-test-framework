package dev.rijksmuseum.dto.artObject;

import lombok.Data;

import java.util.List;

@Data
public class PrincipalMakerDto {
    private String name;
    private String unFixedName;
    private String placeOfBirth;
    private String dateOfBirth;
    private Object dateOfBirthPrecision;
    private String dateOfDeath;
    private Object dateOfDeathPrecision;
    private String placeOfDeath;
    private List<String> occupation;
    private List<String> roles;
    private String nationality;
    private Object biography;
    private List<String> productionPlaces;
    private String qualification;
    private String labelDesc;
}
