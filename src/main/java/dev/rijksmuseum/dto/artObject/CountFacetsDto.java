package dev.rijksmuseum.dto.artObject;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class CountFacetsDto {
    @JsonAlias("hasimage")
    private Integer hasImage;
    @JsonAlias("ondisplay")
    private Integer onDisplay;
}
