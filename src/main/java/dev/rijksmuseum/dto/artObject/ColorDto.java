package dev.rijksmuseum.dto.artObject;

import lombok.Data;

@Data
public class ColorDto {
    private Integer percentage;
    private String hex;
    private String originalHex;
    private String normalizedHex;
}
