package dev.rijksmuseum.dto.artObject;

import lombok.Data;

import java.util.List;

@Data
public class ArtObjectDto {
    private LinksDto links;
    private String id;
    private String priref;
    private String objectNumber;
    private String language;
    private String title;
    private Object copyrightHolder;
    private ImageDto webImage;
    private List<ColorDto> colors;
    private List<ColorDto> colorsWithNormalization;
    private List<ColorDto> normalizedColors;
    private List<ColorDto> normalized32Colors;
    private List<Object> materialsThesaurus;
    private List<Object> techniquesThesaurus;
    private List<Object> productionPlacesThesaurus;
    private List<String> titles;
    private String description;
    private Object labelText;
    private List<String> objectTypes;
    private List<String> objectCollection;
    private List<Object> makers;
    private List<PrincipalMakerDto> principalMakers;
    private String plaqueDescriptionDutch;
    private String plaqueDescriptionEnglish;
    private String principalMaker;
    private Object artistRole;
    private List<Object> associations;
    private AcquisitionDto acquisition;
    private List<Object> exhibitions;
    private List<String> materials;
    private List<Object> techniques;
    private List<String> productionPlaces;
    private DatingDto dating;
    private ClassificationDto classification;
    private Boolean hasImage;
    private List<Object> historicalPersons;
    private List<Object> inscriptions;
    private List<String> documentation;
    private List<Object> catRefRPK;
    private String principalOrFirstMaker;
    private List<DimensionDto> dimensions;
    private List<Object> physicalProperties;
    private String physicalMedium;
    private String longTitle;
    private String subTitle;
    private String scLabelLine;
    private LabelDto label;
    private Boolean showImage;
    private Boolean permitDownload;
    private String location;
    private ImageDto headerImage;
}
