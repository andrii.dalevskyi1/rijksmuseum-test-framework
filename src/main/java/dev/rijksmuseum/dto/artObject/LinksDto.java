package dev.rijksmuseum.dto.artObject;

import lombok.Data;

@Data
public class LinksDto {
    private String self;
    private String web;
    private String search;
}
