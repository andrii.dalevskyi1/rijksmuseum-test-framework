package dev.rijksmuseum.dto.artObject;

import lombok.Data;

@Data
public class DatingDto {
    private String presentingDate;
    private Integer sortingDate;
    private Integer period;
    private Integer yearEarly;
    private Integer yearLate;
}
