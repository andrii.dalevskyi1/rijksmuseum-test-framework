package dev.rijksmuseum.dto.artObjectPage;

import lombok.Data;

import java.util.List;

@Data
public class ArtObjectPageDto {
    private String id;
    private List<Object> similarPages;
    private String lang;
    private String objectNumber;
    private List<Object> tags;
    private String plaqueDescription;
    private Object audioFile1;
    private Object audioFileLabel1;
    private Object audioFileLabel2;
    private String createdOn;
    private String updatedOn;
    private AdLibOverridesDto adlibOverrides;
}
