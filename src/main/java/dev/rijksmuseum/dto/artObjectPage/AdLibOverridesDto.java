package dev.rijksmuseum.dto.artObjectPage;

import lombok.Data;

@Data
public class AdLibOverridesDto {
    private String titel;
    private String maker;
    private String etiketText;
}
