package dev.rijksmuseum.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;


public class PropertyHandler {
    private static final Properties PROPERTIES = new Properties();
    private static final String PROPERTY_FILENAME = "config.properties";

    static {
        try (InputStream input = PropertyHandler.class.getClassLoader().getResourceAsStream(PROPERTY_FILENAME)) {
            PROPERTIES.load(input);
        } catch (IOException ex) {
            throw new RuntimeException("App can't find `" + PROPERTY_FILENAME + "` file. Pls create it");
        }
    }

    private PropertyHandler() {
    }

    public static String getBaseUrl() {
        return PROPERTIES.getProperty("url.base");
    }

    public static String getCollectionUrl() {
        return PROPERTIES.getProperty("url.collection");
    }

    public static String getEnCulturesUrl() {
        return PROPERTIES.getProperty("url.culture.en");
    }

    public static String getApiKey() {
        String apiKey = "API_KEY";
        String result = Optional
                .ofNullable(System.getProperty(apiKey))
                .orElse(System.getenv(apiKey));
        return Optional
                .ofNullable(result)
                .orElseThrow(() -> new RuntimeException("API_KEY is not set"));
    }

}
