package dev.rijksmuseum.utils;

import java.io.IOException;
import java.io.InputStream;

import static dev.rijksmuseum.utils.PojoConverter.convertInputStreamToPojo;
import static java.lang.String.format;

public class FileReader {

    public static <T> T readTestJsonFileAsPojo(String fileName, Class<T> clazz) {
        String filePath = format("testdata/expectedresults/%s", fileName);
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = loader.getResourceAsStream(filePath);
        try {
            return convertInputStreamToPojo(inputStream, clazz);
        } catch (IOException e) {
            throw new RuntimeException(
                    format("Couldn't find or parse %s file. Pls check it", filePath), e);
        }
    }
}
