package dev.rijksmuseum.utils;


import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;

public class PojoConverter {
    private static final ObjectMapper mapper = new ObjectMapper();

    private PojoConverter() {
    }

    public static <T> T convertInputStreamToPojo(InputStream inputStream, Class<T> clazz) throws IOException {
        return mapper.readValue(inputStream, clazz);
    }
}
