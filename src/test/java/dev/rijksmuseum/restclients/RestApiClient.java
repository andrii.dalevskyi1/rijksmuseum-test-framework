package dev.rijksmuseum.restclients;

import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;

import java.util.Map;


public class RestApiClient {
    private final RequestSpecification requestSpecification;

    public RestApiClient(RequestSpecification requestSpecification) {
        this.requestSpecification = SerenityRest.given()
                .spec(requestSpecification);
    }

    public void makeGetRequest(String endpointUrl, Map<String, Object> params) {
        requestSpecification
                .when()
                    .params(params)
                    .log().all()
                    .get(endpointUrl)
                .then()
                    .log().all();
    }

}
