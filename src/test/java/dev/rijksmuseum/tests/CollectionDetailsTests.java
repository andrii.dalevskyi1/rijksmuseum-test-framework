package dev.rijksmuseum.tests;

import dev.rijksmuseum.actions.CollectionDetailsActions;
import dev.rijksmuseum.dto.CollectionDetailResponseDto;
import net.serenitybdd.annotations.Steps;
import net.serenitybdd.annotations.Title;
import net.serenitybdd.annotations.WithTag;
import net.serenitybdd.junit5.SerenityJUnit5Extension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static dev.rijksmuseum.utils.FileReader.readTestJsonFileAsPojo;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;

@ExtendWith(SerenityJUnit5Extension.class)
public class CollectionDetailsTests {

    @Steps
    CollectionDetailsActions collectionDetailsActions;

    @Test
    @Title("Check valid 'object-number' param")
    public void checkValidObjectNumberParam() {
        String objectNumber = "SK-A-3580";
        CollectionDetailResponseDto expectedResponseDto = readTestJsonFileAsPojo(
                "collectiondetails/SK-A-3580.json",
                CollectionDetailResponseDto.class);
        collectionDetailsActions.makeGetCollectionDetailsEnRequest(objectNumber);
        collectionDetailsActions.statusCodeShouldBe(SC_OK);
        collectionDetailsActions.responseArtObjectShouldBeMatched(expectedResponseDto);
    }

    @WithTag("bug")
    //BUG: The test is failing because the response is 500 but, it should be 404
    @Test
    @Title("Check invalid 'object-number' param")
    public void checkInvalidObjectNumberParam() {
        String objectNumber = "invalid";
        collectionDetailsActions.makeGetCollectionDetailsEnRequest(objectNumber);
        collectionDetailsActions.statusCodeShouldBe(SC_NOT_FOUND);
        collectionDetailsActions.responseShouldBeErrorNotFound();
    }

}
