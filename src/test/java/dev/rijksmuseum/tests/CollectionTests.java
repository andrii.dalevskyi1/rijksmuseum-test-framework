package dev.rijksmuseum.tests;

import dev.rijksmuseum.actions.CollectionActions;
import net.serenitybdd.annotations.Steps;
import net.serenitybdd.annotations.Title;
import net.serenitybdd.annotations.WithTag;
import net.serenitybdd.junit5.SerenityJUnit5Extension;
import net.serenitybdd.rest.SerenityRest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Collections;
import java.util.Optional;

import static dev.rijksmuseum.utils.PropertyHandler.*;
import static java.lang.String.format;
import static org.apache.http.HttpStatus.*;

@ExtendWith(SerenityJUnit5Extension.class)
public class CollectionTests {

    @Steps
    CollectionActions collectionActions;

    @ParameterizedTest
    @CsvSource({"key,invalid",
            "key,",
            ","})
    @Title("Check Not Valid Api 'key' param}")
    public void checkNotValidApiKeyParam(String key, String value) {
        Optional.ofNullable(key)
                .ifPresentOrElse(
                        k -> collectionActions.makeGetCollectionEnRequest(Collections.singletonMap(k, value)),
                        () -> SerenityRest
                                .given()
                                    .baseUri(getBaseUrl())
                                .when()
                                    .get(format("%s/%s", getEnCulturesUrl(), getCollectionUrl())));
        collectionActions.statusCodeShouldBe(SC_UNAUTHORIZED);
    }

    @ParameterizedTest
    @CsvSource({"json,application/json",
            "jsonp,text/javascript",
            "xml,application/xml"})
    @Title("Check Valid 'format' param")
    void checkValidFormatParam(String formatParam, String expectedContentType) {
        collectionActions.makeGetCollectionEnRequest(Collections.singletonMap("format", formatParam));
        collectionActions.statusCodeShouldBe(SC_OK);
        collectionActions.responseContentTypeShouldBe(expectedContentType);
        collectionActions.responseBodyTypeShouldBe(formatParam);
    }

    @Test
    @Title("Check invalid 'format' param")
    public void checkInvalidFormatParam() {
        collectionActions.makeGetCollectionEnRequest(Collections.singletonMap("format", "invalid"));
        collectionActions.statusCodeShouldBe(SC_NOT_FOUND);
        collectionActions.responseShouldBeErrorNotFound();
    }

    @Test
    @Title("Check valid 'page' param")
    public void checkValidPageNumberParam() {
        collectionActions.makeGetCollectionEnRequest(Collections.singletonMap("p", 5));
        collectionActions.statusCodeShouldBe(SC_OK);
        collectionActions.responseBodyTypeShouldByNotEmpty();
    }

    @Test
    @Title("Check default 'page' param")
    public void checkDefaultPageNumberParam() {
        collectionActions.makeGetCollectionEnRequest();
        collectionActions.statusCodeShouldBe(SC_OK);
        collectionActions.responseBodyTypeShouldByNotEmpty();
    }

    @WithTag("bug")
    //BUG: The test is not working as expected. The test should check the boundary limit of the 'page' param.
    @ParameterizedTest
    @CsvSource({"10001",
            "-1"})
    @Title("Check Not Valid 'page' param")
    public void checkPageNumberBoundaryLimit(String pageNumber) {
        collectionActions.makeGetCollectionEnRequest(Collections.singletonMap("p", pageNumber));
        collectionActions.statusCodeShouldBe(SC_BAD_REQUEST);
    }

    @ParameterizedTest
    @CsvSource({"nl",
            "en"})
    @Title("Check 'culture' type")
    public void checkCultureType(String culture) {
        collectionActions.makeGetCollectionRequest(culture);
        collectionActions.statusCodeShouldBe(SC_OK);
        collectionActions.responseCultureShouldBe(culture);
    }

    @Test
    @Title("Check invalid 'culture' type")
    public void checkInvalidCultureParam() {
        collectionActions.makeGetCollectionRequest("invalid");
        collectionActions.statusCodeShouldBe(SC_NOT_FOUND);
    }
}
