package dev.rijksmuseum.actions;

import dev.rijksmuseum.dto.CollectionDetailResponseDto;
import net.serenitybdd.annotations.Step;

import java.util.Collections;
import java.util.Map;

import static dev.rijksmuseum.utils.PropertyHandler.getCollectionUrl;
import static dev.rijksmuseum.utils.PropertyHandler.getEnCulturesUrl;
import static java.lang.String.format;
import static net.serenitybdd.rest.SerenityRest.then;
import static org.assertj.core.api.Assertions.assertThat;

public class CollectionDetailsActions extends BasicActions {

    @Step("Make GET /en/collection/{0} request")
    public void makeGetCollectionDetailsEnRequest(String stringStringMap) {
        getCollectionObjectRequest(getEnCulturesUrl(), stringStringMap, Collections.emptyMap());
    }

    @Step("Check that response art object is as expected")
    public void responseArtObjectShouldBeMatched(CollectionDetailResponseDto expectedResponseDto) {
        CollectionDetailResponseDto actualResponseDto = then()
                .extract()
                .body()
                .as(CollectionDetailResponseDto.class);
        assertThat(actualResponseDto)
                .as("Art object response is not as expected")
                .isEqualTo(expectedResponseDto);
    }

    private void getCollectionObjectRequest(String culture, String objectNumber, Map<String, Object> requestParams) {
        restApiClient.makeGetRequest(
                format("%s/%s/%s", culture, getCollectionUrl(), objectNumber),
                requestParams);
    }
}
