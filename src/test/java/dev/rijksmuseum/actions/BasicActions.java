package dev.rijksmuseum.actions;

import dev.rijksmuseum.dto.ErrorResponseDto;
import dev.rijksmuseum.restclients.RestApiClient;
import io.restassured.builder.RequestSpecBuilder;
import net.serenitybdd.annotations.Step;
import net.serenitybdd.core.steps.UIInteractions;
import org.assertj.core.api.SoftAssertions;

import static dev.rijksmuseum.utils.PropertyHandler.getApiKey;
import static dev.rijksmuseum.utils.PropertyHandler.getBaseUrl;
import static java.lang.String.format;
import static net.serenitybdd.rest.SerenityRest.then;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class BasicActions extends UIInteractions {

    protected final RestApiClient restApiClient = new RestApiClient(
            new RequestSpecBuilder()
                    .addParam("key", getApiKey())
                    .setBaseUri(getBaseUrl())
                    .build());

    @Step("Check that response status code is {0}")
    public void statusCodeShouldBe(Integer statusCode) {
        then().assertThat().statusCode(statusCode);
    }

    @Step("Check that response Content-Type is {0}")
    public void responseContentTypeShouldBe(String expectedContentType) {
        then().assertThat().contentType(expectedContentType);
    }

    @Step("Check that response Body Type is {0}")
    public void responseBodyTypeShouldBe(String formatParam) {
        String actualResponseWithoutSpecChars = then()
                .assertThat()
                .extract()
                .body()
                .asString()
                .replaceAll("\\p{C}", "");
        switch (formatParam) {
            case "json", "jsonp":
                assertThat(actualResponseWithoutSpecChars)
                        .as(format("The response body is not %s", formatParam))
                        .startsWith("{");
                break;
            case "xml":
                assertThat(actualResponseWithoutSpecChars)
                        .as(format("The response body is not %s", formatParam))
                        .startsWith("<");
                break;
        }
    }

    @Step("Check that response Body Type is not empty")
    public void responseBodyTypeShouldByNotEmpty() {
        assertThat(then().extract().body().asString())
                .as("The response body is empty")
                .isNotEmpty();
    }

    @Step("Check that response is error Not Found")
    public void responseShouldBeErrorNotFound() {
        ErrorResponseDto errorResponse = then().extract().as(ErrorResponseDto.class);
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(errorResponse.getStatus())
                    .as("The error status is not 404")
                    .isEqualTo(404);
            softly.assertThat(errorResponse.getTitle())
                    .as("The error title is not 'Not Found'")
                    .isEqualTo("Not Found");
        });
    }
}
