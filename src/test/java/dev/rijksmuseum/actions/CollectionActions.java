package dev.rijksmuseum.actions;

import dev.rijksmuseum.dto.CollectionResponseDto;
import net.serenitybdd.annotations.Step;

import java.util.Collections;
import java.util.Map;

import static dev.rijksmuseum.utils.PropertyHandler.getCollectionUrl;
import static dev.rijksmuseum.utils.PropertyHandler.getEnCulturesUrl;
import static java.lang.String.format;
import static net.serenitybdd.rest.SerenityRest.then;
import static org.assertj.core.api.Assertions.assertThat;

public class CollectionActions extends BasicActions {

    @Step("Make GET /en/collection request with params: {0}")
    public void makeGetCollectionEnRequest(Map<String, Object> requestParams) {
        getCollectionRequest(getEnCulturesUrl(), requestParams);
    }

    @Step("Make GET /en/collection request without params")
    public void makeGetCollectionEnRequest() {
        getCollectionRequest(getEnCulturesUrl(), Collections.emptyMap());
    }

    @Step("Make GET /{0}/collection request without params}")
    public void makeGetCollectionRequest(String cultures) {
        getCollectionRequest(cultures, Collections.emptyMap());
    }

    @Step("Check that response culture is {0}")
    public void responseCultureShouldBe(String expectedCulture) {
        CollectionResponseDto collectionResponseDto = then()
                .extract()
                .body()
                .as(CollectionResponseDto.class);
        String actualCulture = collectionResponseDto.getArtObjects().get(0).getId().substring(0, 2);
        assertThat(actualCulture)
                .as("Culture is not as expected")
                .isEqualToIgnoringCase(expectedCulture);
    }

    private void getCollectionRequest(String culture, Map<String, Object> requestParams) {
        restApiClient.makeGetRequest(
                format("%s/%s", culture, getCollectionUrl()),
                requestParams);
    }

}
